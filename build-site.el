(require 'ox-publish)

(setq org-html-validation-link nil            ;; Pas de création de lien de validation W3
      org-export-with-sub-superscripts nil    ;; Affichage correct des _ pour l'export HTML
      org-html-htmlize-output-type 'css       ;; Utilisation d'un css externe pour la coloration syntaxique des blocs de code
      org-html-head-extra "<link rel=\"stylesheet\" href=\"css/org-src-doom-palenight.css\" />")

(setq org-publish-project-alist
      '(;; Sortie «reveal.js»
        ("reveal"
         :base-directory "./content"
         :exclude "index.org"
         :publishing-function org-reveal-publish-to-reveal
         :publishing-directory "./public" ; Utilisation d'un répertoire différent
         :language "en")

        ("lib-reveal"
         :recursive t
         :base-directory "./content/lib/reveal.js"
         :base-extension "css\\|scss\\|js\\|ttf"
         :publishing-function org-publish-attachment
         :publishing-directory "./public/lib/reveal.js")

        ("css"
         :base-directory "./content/css"
         :base-extension "css"
         :publishing-function org-publish-attachment
         :publishing-directory "./public/css")

        ;; Copie des images
        ("images"
         :base-directory "./content/img"
         :base-extension "png\\|jpg"
         :publishing-function org-publish-attachment
         :publishing-directory "./public/img")))
(org-publish-all t)

(message "Build complete!")
